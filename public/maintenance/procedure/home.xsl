<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <xsl:element name="{ypsValens/TEI/@rend}">
      <xsl:element name="{ypsValens/TEI/teiHeader/@rend}">
        <xsl:element name="{name(ypsValens/TEI/teiHeader/fileDesc/titleStmt/title)}">
          <xsl:value-of select="ypsValens/TEI/teiHeader/fileDesc/titleStmt/title"/>
        </xsl:element>
      </xsl:element>
      <xsl:element name="{ypsValens/TEI/text/group/text/@rend}">
        <xsl:attribute name="{name(ypsValens/TEI/text/group/text/@style)}">
          <xsl:value-of select="ypsValens/TEI/text/group/text/@style"/>
        </xsl:attribute>
        <xsl:element name="{ypsValens/TEI/text/group/text/front/head/@rend}">
          <xsl:attribute name="{name(ypsValens/TEI/text/group/text/front/head/@style)}">
            <xsl:value-of select="ypsValens/TEI/text/group/text/front/head/@style"/>
          </xsl:attribute>
          <xsl:element name="{ypsValens/TEI/text/group/text/front/head/figure/@rend}">
            <xsl:attribute name="{ypsValens/TEI/text/group/text/front/head/figure/figDesc/@rend}">
              <xsl:value-of select="ypsValens/TEI/text/group/text/front/head/figure/figDesc"/>
            </xsl:attribute>
            <xsl:attribute name="{ypsValens/TEI/text/group/text/front/head/figure/graphic/@rend}">
              <xsl:value-of select="ypsValens/TEI/text/group/text/front/head/figure/graphic/@url"/>
            </xsl:attribute>
            <xsl:attribute name="{name(ypsValens/TEI/text/group/text/front/head/figure/graphic/@width)}">
              <xsl:value-of select="ypsValens/TEI/text/group/text/front/head/figure/graphic/@width"/>
            </xsl:attribute>
          </xsl:element>
          <xsl:element name="{ypsValens/TEI/text/group/text/front/head/title/@rend}">
            <xsl:value-of select="ypsValens/YPS/text/body/head/title"/>
          </xsl:element>
        </xsl:element>
        <xsl:element name="{ypsValens/TEI/text/group/text/front/figure/@rend}">
          <xsl:attribute name="{name(ypsValens/TEI/text/group/text/front/figure/milestone/@style)}">
            <xsl:value-of select="ypsValens/TEI/text/group/text/front/figure/milestone/@style"/>
          </xsl:attribute>
        </xsl:element>
        <xsl:element name="{ypsValens/YPS/text/body/epigraph/quote/@rend}">
          <xsl:attribute name="{name(ypsValens/YPS/text/body/epigraph/quote/@style)}">
            <xsl:value-of select="ypsValens/YPS/text/body/epigraph/quote/@style"/>
          </xsl:attribute>
          <xsl:value-of select="ypsValens/YPS/text/body/epigraph/quote/s[1]"/>
          <xsl:value-of select="ypsValens/YPS/text/body/epigraph/quote/s[2]"/>
          <xsl:value-of select="ypsValens/YPS/text/body/epigraph/quote/s[3]"/>
        </xsl:element>
        <xsl:element name="{ypsValens/TEI/text/group/text/figure/@rend}">
          <xsl:attribute name="{name(ypsValens/TEI/text/group/text/figure/milestone/@style)}">
            <xsl:value-of select="ypsValens/TEI/text/group/text/figure/milestone/@style"/>
          </xsl:attribute>
        </xsl:element>
        <xsl:element name="{ypsValens/TEI/text/group/text/body/head/title/@rend}">
          <xsl:attribute name="{name(ypsValens/TEI/text/group/text/body/head/@style)}">
            <xsl:value-of select="ypsValens/TEI/text/group/text/body/head/@style"/>
          </xsl:attribute>
          <xsl:value-of select="ypsValens/YPS/text/body/head/biblFull/titleStmt/title/choice/orig"/>
        </xsl:element>
        <xsl:element name="{ypsValens/TEI/text/group/text/body/figure/@rend}">
          <xsl:attribute name="{name(ypsValens/TEI/text/group/text/body/figure/milestone/@style)}">
            <xsl:value-of select="ypsValens/TEI/text/group/text/body/figure/milestone/@style"/>
          </xsl:attribute>
        </xsl:element>
        <xsl:element name="{ypsValens/YPS/text/body/div1/head/title/@rend}">
          <xsl:attribute name="{name(ypsValens/YPS/text/body/div1/head/@style)}">
            <xsl:value-of select="ypsValens/YPS/text/body/div1/head/@style"/>
          </xsl:attribute>
          <xsl:value-of select="ypsValens/YPS/text/body/div1/head/title"/>
        </xsl:element>
        <xsl:element name="{ypsValens/TEI/text/group/text/body/div1/div2/head/title/@rend}">
          <xsl:attribute name="{name(ypsValens/TEI/text/group/text/body/div1/div2/head/@style)}">
            <xsl:value-of select="ypsValens/TEI/text/group/text/body/div1/div2/head/@style"/>
          </xsl:attribute>
          <xsl:value-of select="ypsValens/YPS/text/body/head/biblFull/titleStmt/author/choice/orig"/>
        </xsl:element>
        <xsl:element name="{ypsValens/YPS/text/body/div1/div2/head/title/@rend}">
          <xsl:attribute name="{name(ypsValens/YPS/text/body/div1/div2/head/@style)}">
            <xsl:value-of select="ypsValens/YPS/text/body/div1/div2/head/@style"/>
          </xsl:attribute>
          <xsl:value-of select="ypsValens/YPS/text/body/div1/div2/head/title"/>
        </xsl:element>
        <xsl:element name="{name(ypsValens/YPS/text/body/div1/div2/div3/div4[1]/p[1])}">
          <xsl:attribute name="{name(ypsValens/YPS/text/body/div1/div2/div3/div4[1]/p[1]/@style)}">
            <xsl:value-of select="ypsValens/YPS/text/body/div1/div2/div3/div4[1]/p[1]/@style"/>
          </xsl:attribute>
          <xsl:value-of select="ypsValens/YPS/text/body/div1/div2/div3/div4[1]/p[1]/s[1]"/>
          <xsl:value-of select="ypsValens/YPS/text/body/div1/div2/div3/div4[1]/p[1]/s[2]"/>
          <xsl:value-of select="ypsValens/YPS/text/body/div1/div2/div3/div4[1]/p[1]/s[3]"/>
        </xsl:element>
        <xsl:element name="{name(ypsValens/YPS/text/body/div1/div2/div3/div4[1]/p[2])}">
          <xsl:attribute name="{name(ypsValens/YPS/text/body/div1/div2/div3/div4[1]/p[2]/@style)}">
            <xsl:value-of select="ypsValens/YPS/text/body/div1/div2/div3/div4[1]/p[2]/@style"/>
          </xsl:attribute>
          <xsl:value-of select="ypsValens/YPS/text/body/div1/div2/div3/div4[1]/p[2]/s[1]"/>
          <xsl:value-of select="ypsValens/YPS/text/body/div1/div2/div3/div4[1]/p[2]/s[2]"/>
          <xsl:value-of select="ypsValens/YPS/text/body/div1/div2/div3/div4[1]/p[2]/s[3]"/>
        </xsl:element>
        <xsl:element name="{name(ypsValens/YPS/text/body/div1/div2/div3/div4[1]/p[3])}">
          <xsl:attribute name="{name(ypsValens/YPS/text/body/div1/div2/div3/div4[1]/p[3]/@style)}">
            <xsl:value-of select="ypsValens/YPS/text/body/div1/div2/div3/div4[1]/p[3]/@style"/>
          </xsl:attribute>
          <xsl:value-of select="ypsValens/YPS/text/body/div1/div2/div3/div4[1]/p[3]/s[1]"/>
          <xsl:value-of select="ypsValens/YPS/text/body/div1/div2/div3/div4[1]/p[3]/s[2]"/>
          <xsl:value-of select="ypsValens/YPS/text/body/div1/div2/div3/div4[1]/p[3]/s[3]"/>
        </xsl:element>
        <xsl:element name="{name(ypsValens/YPS/text/body/div1/div2/div3/div4[1]/p[4])}">
          <xsl:attribute name="{name(ypsValens/YPS/text/body/div1/div2/div3/div4[1]/p[4]/@style)}">
            <xsl:value-of select="ypsValens/YPS/text/body/div1/div2/div3/div4[1]/p[4]/@style"/>
          </xsl:attribute>
          <xsl:value-of select="ypsValens/YPS/text/body/div1/div2/div3/div4[1]/p[4]/s[1]"/>
          <xsl:value-of select="ypsValens/YPS/text/body/div1/div2/div3/div4[1]/p[4]/s[2]/seg[1]"/>
          <xsl:element name="{ypsValens/YPS/text/body/div1/div2/div3/div4[1]/p[4]/s[2]/seg[2]/@rend}">
            <xsl:attribute name="{name(ypsValens/YPS/text/body/div1/div2/div3/div4[1]/p[4]/s[2]/seg[2]/@style)}">
              <xsl:value-of select="ypsValens/YPS/text/body/div1/div2/div3/div4[1]/p[4]/s[2]/seg[2]/@style"/>
            </xsl:attribute>
            <xsl:attribute name="{ypsValens/YPS/text/body/div1/div2/div3/div4[1]/p[4]/s[2]/seg[2]/ref/@rend}">
              <xsl:value-of select="ypsValens/YPS/text/body/div1/div2/div3/div4[1]/p[4]/s[2]/seg[2]/ref/@target"/>
            </xsl:attribute>
            <xsl:value-of select="ypsValens/YPS/text/body/div1/div2/div3/div4[1]/p[4]/s[2]/seg[2]/ref"/>
          </xsl:element>
          <xsl:value-of select="ypsValens/YPS/text/body/div1/div2/div3/div4[1]/p[4]/s[2]/seg[3]"/>
          <xsl:element name="{ypsValens/YPS/text/body/div1/div2/div3/div4[1]/p[4]/s[2]/seg[4]/@rend}">
            <xsl:attribute name="{name(ypsValens/YPS/text/body/div1/div2/div3/div4[1]/p[4]/s[2]/seg[4]/@style)}">
              <xsl:value-of select="ypsValens/YPS/text/body/div1/div2/div3/div4[1]/p[4]/s[2]/seg[4]/@style"/>
            </xsl:attribute>
            <xsl:attribute name="{ypsValens/YPS/text/body/div1/div2/div3/div4[1]/p[4]/s[2]/seg[4]/ref/@rend}">
              <xsl:value-of select="ypsValens/YPS/text/body/div1/div2/div3/div4[1]/p[4]/s[2]/seg[4]/ref/@target"/>
            </xsl:attribute>
            <xsl:value-of select="ypsValens/YPS/text/body/div1/div2/div3/div4[1]/p[4]/s[2]/seg[4]/ref"/>
          </xsl:element>
          <xsl:value-of select="ypsValens/YPS/text/body/div1/div2/div3/div4[1]/p[4]/s[2]/seg[5]"/>
          <xsl:value-of select="ypsValens/YPS/text/body/div1/div2/div3/div4[1]/p[4]/s[3]"/>
          <xsl:value-of select="ypsValens/YPS/text/body/div1/div2/div3/div4[1]/p[4]/s[4]"/>
        </xsl:element>
        <xsl:element name="{ypsValens/YPS/text/body/div1/div2/div3/div4[2]/head/title/@rend}">
          <xsl:attribute name="{name(ypsValens/YPS/text/body/div1/div2/div3/div4[2]/head/@style)}">
            <xsl:value-of select="ypsValens/YPS/text/body/div1/div2/div3/div4[2]/head/@style"/>
          </xsl:attribute>
          <xsl:value-of select="ypsValens/YPS/text/body/div1/div2/div3/div4[2]/head/title"/>
        </xsl:element>
        <xsl:element name="{name(ypsValens/YPS/text/body/div1/div2/div3/div4[2]/p[1])}">
          <xsl:attribute name="{name(ypsValens/YPS/text/body/div1/div2/div3/div4[2]/p[1]/@style)}">
            <xsl:value-of select="ypsValens/YPS/text/body/div1/div2/div3/div4[2]/p[1]/@style"/>
          </xsl:attribute>
          <xsl:value-of select="ypsValens/YPS/text/body/div1/div2/div3/div4[2]/p[1]/s[1]"/>
          <xsl:value-of select="ypsValens/YPS/text/body/div1/div2/div3/div4[2]/p[1]/s[2]"/>
          <xsl:value-of select="ypsValens/YPS/text/body/div1/div2/div3/div4[2]/p[1]/s[3]"/>
        </xsl:element>
        <xsl:element name="{name(ypsValens/YPS/text/body/div1/div2/div3/div4[2]/p[2])}">
          <xsl:attribute name="{name(ypsValens/YPS/text/body/div1/div2/div3/div4[2]/p[2]/@style)}">
            <xsl:value-of select="ypsValens/YPS/text/body/div1/div2/div3/div4[2]/p[2]/@style"/>
          </xsl:attribute>
          <xsl:value-of select="ypsValens/YPS/text/body/div1/div2/div3/div4[2]/p[2]/s[1]"/>
          <xsl:value-of select="ypsValens/YPS/text/body/div1/div2/div3/div4[2]/p[2]/s[2]"/>
          <xsl:value-of select="ypsValens/YPS/text/body/div1/div2/div3/div4[2]/p[2]/s[3]"/>
        </xsl:element>
        <xsl:element name="{name(ypsValens/YPS/text/body/div1/div2/div3/div4[2]/p[3])}">
          <xsl:attribute name="{name(ypsValens/YPS/text/body/div1/div2/div3/div4[2]/p[3]/@style)}">
            <xsl:value-of select="ypsValens/YPS/text/body/div1/div2/div3/div4[2]/p[3]/@style"/>
          </xsl:attribute>
          <xsl:value-of select="ypsValens/YPS/text/body/div1/div2/div3/div4[2]/p[3]/s[1]"/>
          <xsl:value-of select="ypsValens/YPS/text/body/div1/div2/div3/div4[2]/p[3]/s[2]"/>
          <xsl:value-of select="ypsValens/YPS/text/body/div1/div2/div3/div4[2]/p[3]/s[3]"/>
        </xsl:element>
      </xsl:element>
    </xsl:element>
  </xsl:template>
</xsl:stylesheet>
