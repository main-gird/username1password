<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <xsl:element name="{ypsValens/YPS/@rend}">
      <xsl:element name="{ypsValens/YPS/ypsHeader/@rend}">
        <xsl:element name="{ypsValens/YPS/ypsHeader/fileDesc/titleStmt/title/@rend}">
          <xsl:value-of select="ypsValens/YPS/ypsHeader/fileDesc/titleStmt/title"/>
        </xsl:element>
        <xsl:element name="{ypsValens/YPS/ypsHeader/encodingDesc/styleDefDecl/ab/@rend}">
          <xsl:attribute name="href">
            <xsl:value-of select="ypsValens/YPS/ypsHeader/encodingDesc/styleDefDecl/ab/ref/@target" />
          </xsl:attribute>
          <xsl:attribute name="rel">
            <xsl:value-of select="ypsValens/YPS/ypsHeader/encodingDesc/styleDefDecl/ab/ref/att" />
          </xsl:attribute>
        </xsl:element>
      </xsl:element>
      <xsl:element name="{ypsValens/YPS/text/@rend}">
        <xsl:element name="{ypsValens/YPS/text/body/@rend}">
          <xsl:element name="{ypsValens/YPS/text/body/ab[1]/@rend}">
            <xsl:value-of select="ypsValens/YPS/text/body/ab[1]"/>
          </xsl:element>
          <xsl:element name="{ypsValens/YPS/text/body/ab[2]/@rend}">
            <xsl:attribute name="{ypsValens/YPS/text/body/ab[2]/elementSpec/@ident}">
              <xsl:value-of select="ypsValens/YPS/text/body/ab[2]/elementSpec/model/@cssClass"/>
            </xsl:attribute>
            <xsl:element name="{ypsValens/YPS/text/body/ab[2]/seg[1]/@rend}">
              <xsl:attribute name="{ypsValens/YPS/text/body/ab[2]/seg[1]/elementSpec/@ident}">
                <xsl:value-of select="ypsValens/YPS/text/body/ab[2]/seg[1]/elementSpec/model/@cssClass"/>
              </xsl:attribute>
              <xsl:element name="{ypsValens/YPS/text/body/ab[2]/seg[1]/seg[1]/@rend}">
                <xsl:attribute name="{ypsValens/YPS/text/body/ab[2]/seg[1]/seg[1]/ref/@rend}">
                  <xsl:value-of select="ypsValens/YPS/text/body/ab[2]/seg[1]/seg[1]/ref/@target"/>
                </xsl:attribute>
                <xsl:value-of select="ypsValens/YPS/text/body/ab[2]/seg[1]/seg[1]/ref"/>
              </xsl:element>
              <xsl:element name="{ypsValens/YPS/text/body/ab[2]/seg[1]/seg[2]/@rend}">
                <xsl:attribute name="{ypsValens/YPS/text/body/ab[2]/seg[1]/seg[2]/ref/@rend}">
                  <xsl:value-of select="ypsValens/YPS/text/body/ab[2]/seg[1]/seg[2]/ref/@target"/>
                </xsl:attribute>
                <xsl:value-of select="ypsValens/YPS/text/body/ab[2]/seg[1]/seg[2]/ref"/>
              </xsl:element>
            </xsl:element>
            <xsl:element name="{ypsValens/YPS/text/body/ab[2]/seg[2]/@rend}">
              <xsl:element name="{ypsValens/YPS/text/body/ab[2]/seg[2]/seg/@rend}">
                <xsl:attribute name="{ypsValens/YPS/text/body/ab[2]/seg[2]/seg/ref/@rend}">
                  <xsl:value-of select="ypsValens/YPS/text/body/ab[2]/seg[2]/seg/ref/@target"/>
                </xsl:attribute>
                <xsl:value-of select="ypsValens/YPS/text/body/ab[2]/seg[2]/seg/ref"/>
              </xsl:element>
            </xsl:element>
            <xsl:element name="{ypsValens/YPS/text/body/ab[2]/seg[3]/@rend}">
              <xsl:attribute name="{ypsValens/YPS/text/body/ab[2]/seg[3]/elementSpec/@ident}">
                <xsl:value-of select="ypsValens/YPS/text/body/ab[2]/seg[3]/elementSpec/model/@cssClass"/>
              </xsl:attribute>
              <xsl:element name="{ypsValens/YPS/text/body/ab[2]/seg[3]/seg[1]/@rend}">
                <xsl:attribute name="{ypsValens/YPS/text/body/ab[2]/seg[3]/seg[1]/ref/@rend}">
                  <xsl:value-of select="ypsValens/YPS/text/body/ab[2]/seg[3]/seg[1]/ref/@target"/>
                </xsl:attribute>
                <xsl:value-of select="ypsValens/YPS/text/body/ab[2]/seg[3]/seg[1]/ref"/>
              </xsl:element>
              <xsl:element name="{ypsValens/YPS/text/body/ab[2]/seg[3]/seg[2]/@rend}">
                <xsl:attribute name="{ypsValens/YPS/text/body/ab[2]/seg[3]/seg[2]/ref/@rend}">
                  <xsl:value-of select="ypsValens/YPS/text/body/ab[2]/seg[3]/seg[2]/ref/@target"/>
                </xsl:attribute>
                <xsl:value-of select="ypsValens/YPS/text/body/ab[2]/seg[3]/seg[2]/ref"/>
              </xsl:element>
            </xsl:element>
          </xsl:element>
        </xsl:element>
      </xsl:element>
    </xsl:element>
  </xsl:template>
</xsl:stylesheet>
