<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <xsl:element name="{ypsValens/YPS/@rend}">
      <xsl:element name="{ypsValens/YPS/text/@rend}">
        <xsl:attribute name="{name(ypsValens/YPS/text/@style)}">
          <xsl:value-of select="ypsValens/YPS/text/@style"/>
        </xsl:attribute>
        <xsl:element name="{ypsValens/YPS/text/body/@rend}">
          <xsl:for-each select="ypsValens/YPS/text/body/table">
            <xsl:element name="{name()}">
              <xsl:attribute name="style">
                <xsl:value-of select="@style"/>
              </xsl:attribute>
              <xsl:for-each select="row">
                <xsl:element name="{@rend}">
                  <xsl:for-each select="cell">
                    <xsl:element name="{@rend}">
                      <xsl:attribute name="style">
                        <xsl:value-of select="@style"/>
                      </xsl:attribute>
                      <xsl:value-of select="."/>
                    </xsl:element>
                  </xsl:for-each>
                </xsl:element>
              </xsl:for-each>
            </xsl:element>
          </xsl:for-each>
        </xsl:element>
        <div>
          wikipedia.org/wiki/Mathematical_operators_and_symbols_in_Unicode
        </div>
      </xsl:element>
    </xsl:element>
  </xsl:template>
</xsl:stylesheet>
